#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "MovingTypes.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	Aim_State UMETA(DisplayName = "Aim State"),
	Walk_State UMETA(DisplayName = "Walk State"),
	Run_State UMETA(DisplayName = "Run State")
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CharacterMovement)
	float AimSpeed = 75;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CharacterMovement)
	float WalkSpeed = 150;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CharacterMovement)
	float RunSpeed = 500;
};

UCLASS()
class TOPDOWNSHOOTER_API UMovingTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
};