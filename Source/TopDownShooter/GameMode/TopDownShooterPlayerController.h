#pragma once

#include "CoreMinimal.h"
#include "Templates/SubclassOf.h"
#include "GameFramework/PlayerController.h"
#include "InputActionValue.h"
#include "TopDownShooterPlayerController.generated.h"

class UNiagaraSystem;

UCLASS()
class ATopDownShooterPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ATopDownShooterPlayerController();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	float ShortPressThreshold;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	UNiagaraSystem* FXCursor;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	class UInputMappingContext* TopDownShooterMappingContext;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	class UInputAction* MoveAction;

protected:
	void Move(const FInputActionValue& Value);

	void AddPawnMovementInput(const FVector& ForwardDirection, FVector2D& MovementVector, const FVector& RightDirection);

protected:
	uint32 bMoveToMouseCursor : 1;
	APawn* ControlledPawn;

	virtual void SetupInputComponent() override;
	virtual void BeginPlay();

private:
	FVector CachedDestination;

	bool bIsTouch;
	float FollowTime;
};