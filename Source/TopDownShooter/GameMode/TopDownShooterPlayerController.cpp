#include "TopDownShooterPlayerController.h"
#include "GameFramework/Pawn.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "NiagaraSystem.h"
#include "NiagaraFunctionLibrary.h"
#include "TopDownShooter/Character/TopDownShooterCharacter.h"
#include "Engine/World.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "GameFramework/SpringArmComponent.h"

ATopDownShooterPlayerController::ATopDownShooterPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Default;
	CachedDestination = FVector::ZeroVector;
	FollowTime = 0.f;
}

void ATopDownShooterPlayerController::BeginPlay()
{
	Super::BeginPlay();

	if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer()))
	{
		Subsystem->AddMappingContext(TopDownShooterMappingContext, 0);
	}

	ControlledPawn = GetPawn();
}

void ATopDownShooterPlayerController::Move(const FInputActionValue& Value)
{
	if (ControlledPawn != nullptr)
	{
		FVector2D MovementVector = Value.Get<FVector2D>();

		ATopDownShooterCharacter* TopDownShooterCharacter = Cast<ATopDownShooterCharacter>(ControlledPawn);
		USpringArmComponent* CameraBoom = TopDownShooterCharacter->GetCameraBoom();
		FRotator  CameraBoomRotation = CameraBoom->GetRelativeRotation();

		const FVector ForwardDirection = FRotationMatrix(CameraBoomRotation).GetScaledAxis(EAxis::X) + FRotationMatrix(CameraBoomRotation).GetScaledAxis(EAxis::Z);
		const FVector RightDirection = FRotationMatrix(CameraBoomRotation).GetScaledAxis(EAxis::Y);

		AddPawnMovementInput(ForwardDirection, MovementVector, RightDirection);
	}
}

void ATopDownShooterPlayerController::AddPawnMovementInput(const FVector& ForwardDirection, FVector2D& MovementVector, const FVector& RightDirection)
{
	ControlledPawn->AddMovementInput(ForwardDirection, MovementVector.Y);
	ControlledPawn->AddMovementInput(RightDirection, MovementVector.X);
}

void ATopDownShooterPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	if (UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(InputComponent))
	{
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &ATopDownShooterPlayerController::Move);
	}
}
