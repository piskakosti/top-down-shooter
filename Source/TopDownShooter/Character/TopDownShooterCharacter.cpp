#include "TopDownShooterCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"


ATopDownShooterCharacter::ATopDownShooterCharacter()
{
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true);
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false;

	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false;

	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATopDownShooterCharacter::BeginPlay()
{
	Super::BeginPlay();

	CharacterUpdate();
}

void ATopDownShooterCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
	if (bIsAimedAt)
	{
		RotationToCursorByTick(DeltaSeconds);
	}
}

void ATopDownShooterCharacter::RotationToCursorByTick(float DeltaSeconds)
{
	if (APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0))
	{
		FHitResult HitResult;
		PlayerController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, HitResult);

		
		float findYawResult = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), HitResult.Location).Yaw;

		SetActorRotation(FQuat(FRotator(0, findYawResult, 0)));
	}
}

void ATopDownShooterCharacter::CharacterUpdate()
{
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		GetCharacterMovement()->MaxWalkSpeed = MovementInfo.AimSpeed;
		bIsAimedAt = true;
		bIsSprinting = false;
		break;
	case EMovementState::Walk_State:
		GetCharacterMovement()->MaxWalkSpeed = MovementInfo.WalkSpeed;
		bIsAimedAt = false;
		bIsSprinting = false;
		break;
	case EMovementState::Run_State:
		GetCharacterMovement()->MaxWalkSpeed = MovementInfo.RunSpeed;
		bIsAimedAt = false;
		bIsSprinting = true;
		break;
	default:
		break;
	}
}

EMovementState ATopDownShooterCharacter::ChangeMovementState(EMovementState NewMovementState)
{
	MovementState = NewMovementState;
	CharacterUpdate();
	return MovementState;
}

void ATopDownShooterCharacter::SetIsAimedAt(bool NewIsAimedAt)
{
	bIsAimedAt = NewIsAimedAt;
}
